import java.util.Scanner;

public class Game {
	private Board board;
	private Player x;
	private Player o;
	
	
	
	public Game(){
		o = new Player('O');
		x = new Player('X');
		board = new Board(o,x);
	}
	
	public void play() {
		showWelcome();
		while (true) {
			while (true) {
				showTable();
				showTurn();
				input();
				if (board.isFinish() == true) {
					showTable();
					showResult();
					showStatus();
					
					break;
				}
		}	
			clearTable();
	}
}
	private void clearTable() {
		board.setCount();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				board.table[i][j] = '-';
			}
		}
	}

	
	private void showTable() {
		System.out.println("  1 2 3");
		char[][] table = board.getTable();
		for(int i = 0 ; i<3 ; i++) {
			System.out.print(i+1 + " ");
			for(int j =0 ; j<3 ; j++) {
				System.out.print(table[i][j]+ " ");
			}
			System.out.println();
		}
		
	}

	private void showWelcome() {
		System.out.println("Welcom to xo Game");
	}
	private void showTurn() {
		System.out.print(board.getCurrent().getName()+" Turn,");
	}
	private void input() {
		Scanner k =  new Scanner(System.in);
		System.out.print("input Row Col : ");
		int row = k.nextInt();
		int col = k.nextInt();
		System.out.println();
		if (row - 1 >= 0 && row - 1 <= 2 && col - 1 >= 0 && col - 1 <= 2) {
			if (board.getTable()[row - 1][col - 1] == '-') {
				board.setTable(row - 1, col - 1);
				board.switchTurn();
			} else {
				System.out.println("Error, Please Input again.");
			}
		} else {
			System.out.println("Error, Please Input again.");
		}

	}
	private void showStatus() {
		System.out.println("X : Win " + x.getWin() + " Lose " + x.getLose() + " Draw " + x.getDraw());
		System.out.println("O : Win " + o.getWin() + " Lose " + o.getLose() + " Draw " + o.getDraw());
		System.out.println();
	}
	private void showResult() {
		System.out.println(board.getWinner().getName() + " Win!!");
	}
	
	
}